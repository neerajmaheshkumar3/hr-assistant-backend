import json
from flask import Flask, request, redirect, make_response, session
from flask_cors import CORS, cross_origin
from Main import *
from flask_jsonpify import jsonify

app = Flask(__name__)


input_data=''
response = None

CORS(app, support_credentials=True)


@app.route("/question", methods=['POST'])
@cross_origin(supports_credentials=True)

def question():  
        global response     
        input_data=request.data.decode()
        print(input_data)
        response=answer_question(df, question=input_data)
        print(response)
        return redirect('/response')

@app.route('/response', methods=['GET'])
def show_response():
     global response   
     print(response)
     if response is None:
        return 'No response available'   
     return response


if __name__ == '__main__':
     app.run(port=5002)
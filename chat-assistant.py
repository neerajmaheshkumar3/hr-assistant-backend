import openai
import PyPDF2
import pandas as pd
import tiktoken
from openai.embeddings_utils import get_embedding
from pathlib import Path

# Set up OpenAI API
openai.api_key = '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'

pdf_search = Path("D:\PWC\HR Assistant\Docs").glob("*.pdf")

pdf_files = [str(file.absolute()) for file in pdf_search]

#print(content)
def remove_newlines(text):
    text = text.replace('\n', ' ')
    text = text.replace('\\n', ' ')
    text = text.replace('  ', ' ')
    return text

def split_into_many(text, max_tokens = 500):
    sentences = text.split('. ')
    n_tokens = [len(tokenizer.encode(" " + sentence)) for sentence in sentences]
    chunks = []
    tokens_so_far = 0
    chunk = []
    for sentence, token in zip(sentences, n_tokens):
        if tokens_so_far + token > max_tokens:
            chunks.append(". ".join(chunk) + ".")
            chunk = []
            tokens_so_far = 0
        if token > max_tokens:
            continue
        chunk.append(sentence)
        tokens_so_far += token + 1
    return chunks



for pdf_path in pdf_files:
    pdfFile = open(pdf_path, 'rb')
    pdfReader = PyPDF2.PdfReader(pdfFile)
    numOfPages = len(pdfReader.pages)
    content_list = []
    for i in range(0,numOfPages):
        pageObj = pdfReader.pages[i]        
        content_list.append(remove_newlines(pageObj.extract_text()))
    pdfFile.close()
    tokenizer = tiktoken.get_encoding("cl100k_base")
    df = pd.DataFrame(columns=['init_content','init_tokens','split_content','recal_tokens','embeddings'])
    df['init_content'] = pd.Series(content_list)
    df['init_tokens'] = df.init_content.apply(lambda x: len(tokenizer.encode(x)))
    shortened = []
    for row in df.iterrows():
        if row[1]['init_content'] is None:
            continue
        if row[1]['init_tokens'] > 500:
            shortened += split_into_many(row[1]['init_content'])
        else:
            shortened.append(row[1]['init_content'])
    df['split_content'] = pd.Series(shortened)
    df['recal_tokens'] = df.split_content.apply(lambda x: len(tokenizer.encode(x)))
    df['embeddings'] = df.split_content.apply(lambda x: get_embedding(x, engine='text-embedding-ada-002'))
    df.to_csv(pdf_path+'.csv', index=False)


